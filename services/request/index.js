import { isEmpty } from "../../services/common";

/**
 * 
 * @param {GET,POST,PUT,DELETE} method 
 * @param {object} data 
 * Construye las opciones para una peticion por fetch
 */
export const buildFetchOptions = (method, data) => {
    let options = {
        method: method,
        redirect: 'manual',
        mode: 'cors',
        cache: 'default'
    }

    if(method === 'POST' || method === 'PUT'){
        options.body = JSON.stringify(data);
        options.headers = {
            'Content-Type': 'application/json',
            'Access-Control-Expose-Headers' : 'Location'
        }
    }

    return options;
}

/**
 * Crea la url para un objeto en especifico que tenga
 * id como llave unica
 * @param {string} baseURL 
 * @param {object} object 
 */
export const buildObjectURL = (baseURL, object) => {
    return baseURL + object.id;
}

/**
 * Construye la url para update, delete y get
 * de un documento en particular
 * @param {string} baseURL
 * @param {string} docType
 * @param {string} refNbr
 */
export const buildDocumentURL = (baseURL, docType, refNbr) =>{
    return  baseURL + 'doctype/' + docType + '/refnbr/' + refNbr;
}

/**
 * 
 * @param {response} response
 * Maneja las respuestas Get y sus posibles errores 
 */
export const handleGetErrors = (response) => {
    if(!response.ok){            
        throw Error(response.statusText);
    }

    return response.json();
}

/**
 * 
 * @param {response} response
 * Maneja las respuestas de Post y sus posibles errores 
 */
export const handlePostErrors = (response) => {
    if(response.ok)
        return response.headers.get('Location');
    else
        throw Error(response.statusText);
}

export const handlePostPDFErrors = (response) => {
    if(response.ok)
        return response;
    else
        throw Error(response.statusText);
}

/**
 * 
 * @param {response} response
 * Maneja las respuestas de Put y sus posibles errores 
 */
export const handlePutDeleteErrors = (response) => {
    if(!response.ok){
        throw Error(response.statusText);
    }

    return true;
}