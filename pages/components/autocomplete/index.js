import React from 'react';

//Components
import { AutoComplete } from 'primereact/autocomplete';

export const Autocomplete = (props) => {
    
    const filterSuggestions = (event) =>{
        
        const result = props.suggestions.filter(option => {
            return option[props.property].includes(event.query);
        });

        props.updateOptions(result);
    }

    return(
        <AutoComplete value={props.value[props.property]} field={props.property} suggestions={props.suggestions} 
        onChange={props.onChange} completeMethod={filterSuggestions} dropdown={true} onSelect={props.onSelected}
        onBlur={props.onBlur}/>
    );

}