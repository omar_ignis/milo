import React, { useState, useEffect } from 'react';
//Functions
import { fetchData } from '../../data/fetch';

import { isEmpty, findObjectByProp } from '../../services/common';
//constants
import { ITEM_TITLE, ITEM_PLACEHOLDER_INVENTORY_METHOD } from '../../data/local/es';
import { INVENTORY_METHODS, ID, ITEM_NAME,  ITEM_INVENTORY_METHOD, DESCRIPTION,} from '../../data/common';

import CONFIG from '../../data/config';
//Components
import { Panel } from 'primereact/panel';
import { Growl } from 'primereact/growl';
import { Menubar } from 'primereact/menubar';
import { Dropdown } from 'primereact/dropdown';
import { InputSwitch } from 'primereact/inputswitch';
import { InputTextarea } from 'primereact/inputtextarea';

// Customs components
import { Autocomplete } from '../components/autocomplete';

const initialState = {
    id: null,
    itemName: null,
    description: '',
    used: false,
    inventoryMethod: INVENTORY_METHODS[0].value,
};

const Item = (props) => {

    const [item, setItem] = useState(initialState);
    const [suggestions, setSuggestions] = useState([]);

    useEffect(() => {
        fetchData(CONFIG.items,setSuggestions);
    }, [])
    
    /* #region Autocomplete */

    const onChange = (event) => {
        
        if(typeof event.value === 'string'){

            if(!isEmpty(event.value)){
                fetchData(`${CONFIG.items}name/${event.value}`, setSuggestions);
            }
            
            updateItem(item, ITEM_NAME, event.value);
        }
        
    }

    const onSelected = (event) => {
        setItem(event.value);
    }

    const onBlur = (event) => {
        const _item = findObjectByProp(suggestions, ITEM_NAME, event.target.value);
        if(!isEmpty(_item)){
            setItem(_item);
        }
        else {
            
            updateItem(initialState, ITEM_NAME, event.target.value);
        }
    }

    /* #endregion Autocomplete */

    const updateItem = (data, property, value) => {
        const _item = {...data};
        _item[property] = value;
        setItem(_item);
    }

    return(
        <div>
            {JSON.stringify(item)}
            <Panel header={ITEM_TITLE}>
                <div className="p-grid" >
                    <div className="p-col">
                        <Autocomplete value={item} updateOptions={setSuggestions} onChange={onChange}
                        suggestions={suggestions} property={ITEM_NAME} onSelected={onSelected} onBlur={onBlur}/>
                    </div>
                </div>
                <div className="p-grid">
                    <div className="p-col">
                        <Dropdown value={ item.inventoryMethod } options={ INVENTORY_METHODS } 
                            placeholder={ITEM_PLACEHOLDER_INVENTORY_METHOD} 
                            onChange={ (e) => {updateItem(item, ITEM_INVENTORY_METHOD, e.target.value)}}/>
                    </div>
                </div>
                <div className="p-grid">
                    <div className="p-col">
                        <InputSwitch checked={item.used} disabled={true} />
                    </div>
                </div>
                <div className="p-grid">
                    <div className="p-col">
                        <InputTextarea rows={8} cols={50} value={item.description} 
                        onChange={e => {updateItem(item, DESCRIPTION, e.target.value)}} />
                    </div>
                </div>
            </Panel>
        </div>
    );
}

export default Item;