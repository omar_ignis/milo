const url = 'http://localhost:8180/InventoryItem';
export default {
    items: `${url}/items/`,
    warehouses: `${url}/warehouses/`,
    iodocuments: `${url}/iodocuments/`,
    labels: 'http://localhost:4001/barcodes',
    post: 'POST',
    put: 'PUT',
    get: 'GET',
    delete: 'DELETE'
}