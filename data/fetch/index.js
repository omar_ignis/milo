import {
    handleGetErrors,
    handlePostErrors,
    buildFetchOptions,
    handlePutDeleteErrors,
    handlePostPDFErrors,
} from '../../services/request';

export const fetchData = (url, update) => {
    fetch(url)
    .then(handleGetErrors)
    .then(response => {
        update(response.content);
    })
}