//Item
export const ACCEPT = "Aceptar";
export const CANCEL = "Cancelar";
export const ADD = "Agregar";
export const REMOVE = "Eliminar";
export const DESC = "Descripción";
export const PRICE = "0.0";

export const ITEM_TITLE = "Articulos";

export const WAREHOUSE_TITLE = "Almacenes";

export const IODOCUMENT_TITLE = "Documentos de Invetario";

//Placeholders
export const NEW_PLACEHOLDER = "Nuevo";
export const DATE_PLACEHOLDER = "dd/mm/yy hh:mm:ss";
//Item placeholders
export const ITEM_PLACEHOLDER_OPTION = "Seleccione un articulo";
export const ITEM_PLACEHOLDER_INVENTORY_METHOD = "Metodo de evaluación";

//Warehouse placeholders
export const WAREHOUSE_PLACEHOLDER_OPTION = "Seleccione un almacen";




export const saveSuccessful = "El registro fue guardado con exito";
export const changeSuccessful = "El cambio fue guardado con exito";
export const deleteSuccessful = "El registro fue eliminado con exito";

export const saveError = "No fue posible guardar el registro nuevo";
export const changeError = "No fue posible guardar el cambio en el registro";
export const deleteError = "No fue posible eliminar el registro";
export const deleteUsedError = "El {0} fue ocupado en una transacción y no es posible eliminarlo";

export const QUANTITY_VALIDATE_ERROR = "Este campo no coincide con la cantidad total";