export const ID = "id";
export const DESCRIPTION = "description";
export const DOC_TYPE = 'docType';
export const REF_NBR = 'refNbr';
export const DOC_DATE = "docDate";
export const QUANTITY = "quantity";
export const UNIT_PRICE = "unitPrice";
export const DISCOUNT = "discount";
export const TOTAL_PRICE ="totalPrice";
export const LABEL = "label";
export const VALUE = "value";
export const LINE_NBR = "lineNbr";
export const STATUS = "status";
//Controls
export const QUANTITY_CONTROL = "quantityControl";

//Item
export const ITEM = "item";
export const ITEM_ID = "itemId";
export const ITEM_NAME = "itemName";
export const ITEM_INVENTORY_METHOD = "inventoryMethod";

//Warehouse
export const WAREHOUSE_ID = "warehouseId";
export const WAREHOUSE_NAME = "warehouseName";

//IODocuments
export const IODOCUMENT_RECEIPT = 'RCP';
export const IODOCUMENT_EXIT = 'OUT'
export const IODOCUMENT_STATUS_OPEN = "O";
export const IODOCUMENT_STATUS_RELEASED = "R";
export const IODOCUMENT_STATUS_HOLD = "H";
export const IODOCUMENT_DOC_TYPES = [
    {label: 'Entrada', value: IODOCUMENT_RECEIPT},
    {label: 'Salida', value: IODOCUMENT_EXIT}
];
export const INVENTORY_METHODS = [
    { label: 'Promedio Ponderado', value: 'AVG' } 
]

export const DOCUMENT_STATUS = [
    { label: 'Abierto', value: IODOCUMENT_STATUS_OPEN },
    { label: 'Liberado', value: IODOCUMENT_STATUS_RELEASED },
    { label: 'Retenido', value: IODOCUMENT_STATUS_HOLD }, 
];

//Class
export const QUANTITY_CONTROL_CLASS = "quantityControlClass";

//url actions
export const RELEASE = "/release";
export const BARCODE = "/barcode";